﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HiVE.WinSHDS_Platform_UnitTest_WinForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormStartup());
        }
    }
}
