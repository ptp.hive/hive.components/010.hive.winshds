﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace HiVE.WinSHDS
{
    public class WinShowShutForm : Form
    {
        #region Properties
        //----------

        private IContainer components;
        private BackgroundWorker op;

        private Form frm { get; [CompilerGenerated] set; }

        //----------
        #endregion Properties

        #region Methods
        //----------

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            base.SuspendLayout();
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x1eb, 0x109);
            base.FormBorderStyle = FormBorderStyle.None;
            base.Name = "ShowShutForm";
            base.ShowIcon = false;
            base.ShowInTaskbar = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Input Form";
            base.TopMost = true;
            base.WindowState = FormWindowState.Maximized;
            base.Load += new EventHandler(this.frmInputForm_Load);
            base.ResumeLayout(false);
        }

        private Bitmap CaptureScreen()
        {
            Bitmap image = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            using (Graphics graphics = Graphics.FromImage(image))
            {
                graphics.CopyFromScreen(0, 0, 0, 0, image.Size);
            }

            return image;
        }

        private static Bitmap MakeGrayScale3(Bitmap original)
        {
            Bitmap image = new Bitmap(original.Width, original.Height);
            Graphics graphics = Graphics.FromImage(image);
            float[][] newColorMatrix = new float[5][];
            newColorMatrix[0] = new float[] { 0.3f, 0.3f, 0.3f, 0f, 0f };
            newColorMatrix[1] = new float[] { 0.59f, 0.59f, 0.59f, 0f, 0f };
            newColorMatrix[2] = new float[] { 0.11f, 0.11f, 0.11f, 0f, 0f };
            float[] numArray2 = new float[5];
            numArray2[3] = 1f;
            newColorMatrix[3] = numArray2;
            numArray2 = new float[5];
            numArray2[4] = 1f;
            newColorMatrix[4] = numArray2;
            ColorMatrix matrix = new ColorMatrix(newColorMatrix);
            ImageAttributes imageAttr = new ImageAttributes();
            imageAttr.SetColorMatrix(matrix);
            graphics.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height), 0, 0, original.Width, original.Height, GraphicsUnit.Pixel, imageAttr);
            graphics.Dispose();

            return image;
        }

        private void op_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(500);
            try
            {
                for (int i = 0; i < 0xcc; i += 7)
                {
                    if (base.Disposing)
                    {
                        return;
                    }
                    base.Opacity = ((double)i) / 200.0;
                    this.Refresh();
                }
            }
            catch { }
        }

        private void frmInputForm_Load(object sender, EventArgs e)
        {
            if (!this.op.IsBusy)
            {
                this.op.RunWorkerAsync();
            }
            this.frm.TopMost = true;
            this.frm.StartPosition = FormStartPosition.CenterScreen;
            this.frm.ShowInTaskbar = false;
            this.DoubleBuffered = true;
            this.frm.ShowDialog();
            base.Close();
        }

        //----------
        #endregion Methods

        #region Get Methods
        //----------

        public WinShowShutForm(Form frmInputForm)
        {
            try
            {
                this.components = null;
                this.op = new BackgroundWorker();
                this.frm = frmInputForm;
                Control.CheckForIllegalCrossThreadCalls = false;
                this.InitializeComponent();
                this.BackgroundImage = MakeGrayScale3(this.CaptureScreen());
                base.Opacity = 0.0;
                this.op.DoWork += new DoWorkEventHandler(this.op_DoWork);
                base.ShowDialog();
            }
            catch { }
        }

        //----------
        #endregion Get Methods
    }
}
