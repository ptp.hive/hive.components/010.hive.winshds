﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HiVE.WinShutDownStyle")]
[assembly: AssemblyDescription("Enable Windows ShutDown Style for the input form.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HiVE Productions")]
[assembly: AssemblyProduct("HiVE.WinSHDS")]
[assembly: AssemblyCopyright("Copyright ©  2016 HiVE Productions Inc. All Rights Reserved.")]
[assembly: AssemblyTrademark("HiVE")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("43edd16b-426f-4e7a-9249-518567a97a0a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("01.00.61012.1404")]
[assembly: AssemblyFileVersion("01.00.61012")]
[assembly: NeutralResourcesLanguage("en")]

